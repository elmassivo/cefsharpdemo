﻿using System;
using CefSharp;
using CefSharp.OffScreen;
using System.IO;

namespace CEFSharpDemo
{
    class CEFSharpDemo_bot2
    {
        private static ChromiumWebBrowser browser;
        const string initialUrl = "https://www.google.com/";

        public static void Main(string[] args)
        {
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            var settings = new CefSettings()
            {
                CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);
            browser = new ChromiumWebBrowser(initialUrl);

            browser.FrameLoadStart += Browser_FrameLoadStart;
            browser.FrameLoadEnd += Browser_FrameLoadEnd;
            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            Console.WriteLine("   ╔══════════════════════════════════════════════════╗");
            Console.WriteLine("   ║             CEFSHARP-BOT #2 ACTIVATED            ║");
            Console.WriteLine("   ╚══════════════════════════════════════════════════╝");

            Console.ReadKey();
            Cef.Shutdown();
        }

        private static void Browser_FrameLoadStart(object sender, FrameLoadStartEventArgs e)
        {
            Console.WriteLine($"{e.Url} loading!");
        }

        static int pageCount = 0;
        private static void Browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            browser.GetSourceAsync().ContinueWith(v =>
            {
                File.WriteAllText($"{pageCount}_page.html", v.Result);
            });

            var screenshot = browser.ScreenshotOrNull();
            screenshot.Save($"{pageCount}_screenShot.png");
            screenshot.Dispose();

            Console.WriteLine($"Saving {pageCount}_screenShot and {pageCount}_page for {e.Url}...");
            pageCount++;
        }

        static int phase = 0;
        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (!e.IsLoading)
            {
                if (phase == 0)
                {
                    // put 'CEFSharp' into the search box and click the 'Search' button
                    var script1 = @"document.querySelector('input[name=q]').value = 'CEFSharp';
                            document.querySelector('input[name=btnK]').click();";
                    browser.EvaluateScriptAsync(script1);
                }
                if (phase == 1)
                {
                    // click the first link
                    var script2 = @"document.querySelector('.r > a').click();";
                    browser.EvaluateScriptAsync(script2);
                }
                phase++;
            }
        }
    }
}
