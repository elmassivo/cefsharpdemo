﻿using System;
using CefSharp;
using CefSharp.OffScreen;
using System.IO;
using System.Threading;

namespace CEFSharpDemo
{
    class CEFSharpDemo_bot3
    {
        private static ChromiumWebBrowser browser;
        const string initialUrl = "https://www.google.com/";
        static bool playAgain = true;

        public static void Main(string[] args)
        {
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            var settings = new CefSettings()
            {
                CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);
            browser = new ChromiumWebBrowser(initialUrl);

            browser.AddressChanged += Browser_AddressChanged;

            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            Console.WriteLine("   ╔══════════════════════════════════════════════════╗");
            Console.WriteLine("   ║             CEFSHARP-BOT #3 ACTIVATED            ║");
            Console.WriteLine("   ╚══════════════════════════════════════════════════╝");

            while (playAgain)
            {
                Thread.Sleep(1000);
            }

            Cef.Shutdown();
        }

        static string currentUrl = "";
        private static void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            currentUrl = e.Address;
        }


        static string fallingUrl = "http://rikukissa.github.io/falling/";
        static string clickScript = "document.querySelector('#start').click()";
        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (!e.IsLoading)
            {
                if (currentUrl != fallingUrl)
                {
                    browser.Load(fallingUrl);
                }
                else
                {
                    browser.ConsoleMessage += Browser_ConsoleMessage;
                    // give the page a moment to finish loading it's javascript
                    Thread.Sleep(500);
                    browser.ExecuteScriptAsync(clickScript);
                }
            }
        }
        private static void Browser_ConsoleMessage(object sender, ConsoleMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
            if(e.Message.Contains("How long it took for you to crash"))
            {
                Console.WriteLine("\nWould you like to play again? (y/n)");
                var result = Console.ReadKey();
                if(result.Key != ConsoleKey.Y)
                {
                    playAgain = false;
                }
                else
                {
                    browser.ExecuteScriptAsync(clickScript);
                }
            }
        }
    }
}
