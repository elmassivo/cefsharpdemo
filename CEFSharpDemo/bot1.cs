﻿using System;
using CefSharp;
using CefSharp.OffScreen;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace CEFSharpDemo
{
    class CEFSharpDemo_bot1
    {
        private static ChromiumWebBrowser browser;
        const string initialUrl = "http://testing-ground.scraping.pro/login";

        public static void Main(string[] args)
        {
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            var settings = new CefSettings()
            {
                CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };
            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);
            browser = new ChromiumWebBrowser(initialUrl);
            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            Console.WriteLine("   ╔══════════════════════════════════════════════════╗");
            Console.WriteLine("   ║             CEFSHARP-BOT #1 ACTIVATED            ║");
            Console.WriteLine("   ╚══════════════════════════════════════════════════╝");

            Console.ReadKey();
            Cef.Shutdown();
        }

        static bool isLoggedIn = false;
        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (!e.IsLoading)
            {
                if (!isLoggedIn)
                {
                    // fill the username and password fields with their respective values, then click the submit button
                    var loginScript = @"document.querySelector('#usr').value = 'admin';
                               document.querySelector('#pwd').value = '12345';
                               document.querySelector('input[type=submit]').click();";

                    browser.EvaluateScriptAsync(loginScript).ContinueWith(u => {
                        isLoggedIn = true;
                        Console.WriteLine("User Logged in.\n");
                    });
                }
                else
                {
                    // push the "success" field and the text from all 'li' elements into an array
                    var pageQueryScript = @"
                    (function(){
                        var lis = document.querySelectorAll('li');
                        var result = [];
                        result.push(document.querySelector('h3.success').innerText);
                        for(var i=0; i < lis.length; i++) { result.push(lis[i].innerText) }
                        return result;
                    })()";

                    var scriptTask = browser.EvaluateScriptAsync(pageQueryScript);
                    scriptTask.ContinueWith(u =>
                    {
                        if (u.Result.Success && u.Result.Result != null)
                        {
                            Console.WriteLine("Bot output recieved!\n\n");
                            var filePath = "output.txt";
                            var response = (List<dynamic>)u.Result.Result;
                            foreach (string v in response)
                            {
                                Console.WriteLine(v);
                            }
                            File.WriteAllLines(filePath, response.Select(v => (string)v).ToArray());
                            Console.WriteLine($"\n\nBot output saved to {filePath}");
                            Console.WriteLine("\n\nPress any key to close.");
                        }
                    });
                }
            }
        }

    }
}
